# encoding: utf-8
require 'pry'
require 'csv'
require 'curb'
require 'nokogiri'

class Product
  def parse(url, file_name)
    next_page_url = url
    index = 0
    while next_page_url != ''
      page = page_data(next_page_url)
      products_urls = page.xpath("//div[@class='width_65']/a[h2]/@href").map(&:text)
      products_urls.each do |url|
        index = product_parse(index, url, file_name)
      end
      next_page_url = page.xpath("//a[@class='next']/@href").text
    end
  end

  private

  def page_data(url)

    if url[0..(ROOT_URL.length-1)] == ROOT_URL
      full_url = url
    else
      full_url = ROOT_URL + url
    end
    data_http = Curl::Easy.http_get(full_url)
    Nokogiri::HTML(data_http.body_str)
  end


  def product_parse(index, url, file_name)
    page_product = page_data(ROOT_URL + url)
    general_name  = page_product.xpath("//h1[@id='product_family_heading']").text
    names  = page_product.xpath("//div[@class='title']").map(&:text)
    prices = page_product.xpath("//span[@itemprop='price']").map(&:text)
    image_url = page_product.xpath("//meta[@property='og:image']/@content").text
    terms_of_delivery = page_product.xpath("//strong[@class='stock in-stock']").map(&:text)
    product_codes = page_product.xpath("//strong[@itemprop='sku']").map(&:text)
    names.each_with_index do |name, i|
      index += 1
      CSV.open(file_name, 'a+') do |csv|
        csv << [ index, "#{general_name} - #{name.strip}", prices[i], image_url,
                 terms_of_delivery[i].strip, product_codes[i] ]
      end
    end
    index
  end
end


