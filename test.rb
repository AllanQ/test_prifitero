#!/usr/bin/env ruby
# encoding: utf-8
require_relative 'lib/product'
require_relative 'lib/params'

ROOT_URL = 'http://www.viovet.co.uk'

array = Params.new(ARGV).parm_array

url = array[0]
filename = array[1]

CSV.open(filename, 'a+') do |csv|
  csv << ['N', 'Название', 'Цена', 'Изображение', 'Срок доставки', 'Код товара']
end


#filename = 'file.svc'
#url = 'http://www.viovet.co.uk/Pet_Foods_Diets-Dogs-Hills_Pet_Nutrition-Hills_Prescription_Diets/c233_234_2678_93/category.html'

Product.new.parse(url, filename)
